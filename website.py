from app import app
from pymongo import Connection
from flask import render_template,flash,request,redirect,url_for,Flask
import os
app.config.from_object(__name__)
app.secret_key="timepass"
#connection = Connection();
#db = connection.enturn

conn = Connection('ds029827.mongolab.com',29827)
db = conn['enturn']
db.authenticate('enturn','enturn')

@app.route('/')
def index():
    interns = db['internships']
    intern = interns.find()
    return render_template('test.html',intern=intern)

@app.route('/add',methods=['POST','GET'])
def addInternship():
    if request.method == 'POST':
        pprint.pprint(request.form)
        intern = {}
        error = False
        intern["title"]=request.form["title"]
        if intern["title"] == "":
            flash('please enter a title for the intership','error')
            error = True
        intern["content"]=request.form["details"]
        if intern["content"] == "":
            flash('please enter a Description for the intership','error')
            error = True
        tags = request.form["tags"]
        intern["tags"] = tags.split(",")
        if (intern["tags"] == "") or (len(intern["tags"]) < 1):
            flash('please add atleast 1 Tag for the intership','error')
            error = True
        intern["companyName"]=request.form["companyName"]
        if intern["companyName"] == "":
            flash('please enter your company name','error')
            error = True
        intern["companyemail"]=request.form["email"]
        if intern["companyemail"] == "":
            flash('please enter a valid company email address','error')
            error = True
        intern["companywebsite"]=request.form["website"]
        if intern["companywebsite"] == "":
            flash('please enter a valid company website','error')
            error = True
        # do something as the form was submitted
        if error == False:
            db.internships.save(intern)
            flash('form submitted! awesome ','success')
            return redirect(url_for('index'))
    return render_template('addintern.html')

if __name__=='__main__':
     port = int(os.environ.get("PORT", 5000))
     app.run(host='0.0.0.0', port=port)
